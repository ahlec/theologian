import sys
import json
import re
import core_theologian

t = core_theologian.login()

############################################
# Handle command line arguments
############################################
if len(sys.argv) < 2:
    print("You must provide a url to use as the end point that monk will trace back to the beginning of the thread.")
    sys.exit(1)
outputFilename = "monk.htm"
if len(sys.argv) >= 3:
    outputFilename = sys.argv[2]
outputFilename = core_theologian.getOutputDir() + "/" + outputFilename

############################################
# Find starting post information
############################################
startRegex = re.compile("https?://([^\\.]*).tumblr.com/post/([0-9]+)")
startMatch = startRegex.match(sys.argv[1])
if startMatch == None:
    print("Could not understand the url you provided.")
    sys.exit(1)
startBlogUrl = startMatch.group(1) + ".tumblr.com"
startPostId = startMatch.group(2)

########
## THREAD DATA STORED AS:
## {
##    text: "",
##    postUrl: "",
##    author: "",
##    date: (date time)
## }
#######

##############################
# Collect the thread!
##############################
thread = []
cleanExit = False

currentPostCall = t.get('posts', blog_url=startBlogUrl, params={ 'id': startPostId, 'reblog_info': True })
while "total_posts" in currentPostCall and currentPostCall["total_posts"] > 0:
    print("collecting... [currently have", len(thread), "posts]")
    currentPost = currentPostCall["posts"][0]
    thread.insert(0, { "text": currentPost["reblog"]["comment"], "postUrl": currentPost["post_url"], "author": currentPost["blog_name"],
        "date": currentPost["date"] })
    if not "reblogged_from_id" in currentPost:
        if currentPost["type"] == "answer":
            # add the origin question as well (this normally just adds the answer)
            thread.insert(0, { "text": currentPost["question"], "postUrl": currentPost["post_url"], "author": currentPost["asking_name"],
                "date": currentPost["date"] })
        cleanExit = True
        break
    currentPostCall = t.get('posts', blog_url=currentPost["reblogged_from_name"] + ".tumblr.com", params={ 'id': currentPost["reblogged_from_id"], 'reblog_info': True })
print("finished with a total of", len(thread), "posts")
    
##############################
# REBUILD!
##############################
outputFile = open(outputFilename, "w")
outputFile.write("""<!DOCTYPE html>
<html>
    <head>
        <style>
            body {
                font-family: Tahoma;
                font-size: 13px;
            }
            table {
                margin-top: 20px;
                border-collapse: collapse;
            }
            td {
                border-top: 1px solid black;
                vertical-align: top;
            }
            
            td.postMeta {
                width: 200px;
                max-width: 200px;
            }
        </style>
    </head>
    <body>""")
if not cleanExit:
    outputFile.write("""<div style="border:2px solid #8B0000; background-color: #F08080; font-weight:bold; text-align:center; padding: 3px;">Something went whacky with trying to
    recover all of the posts. I went as far back as I could, but something broke. I'm sorry, I hope you as a human can fix where I as a dumb computer program failed.</div>""")
outputFile.write("""<table>""")
isFirstPost = True
for post in thread:
    # post meta
    outputFile.write("""<tr><td class="postMeta">""")
    outputFile.write("[<a href=\"" + post["postUrl"] + "\" target=\"_blank\">" + post["author"] + "</a>]")
    outputFile.write("<br />")
    outputFile.write(post["date"])
    outputFile.write("</td>")
    
    # post itself
    outputFile.write("<td>")
    outputFile.write(post["text"])
    outputFile.write("</td></tr>")
outputFile.write("""    </table>
    </body>
</html>""")
outputFile.close()

print("all done! check out:", outputFilename)