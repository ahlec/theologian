# Theologian #

This is a suite of tools designed to aid Tumblr bloggers. In short, this leverages the Tumblr API in a way that's meant to fill in the gaps of functionality. The suite operates together off of a central core, designed to be easy to use and easy to expand so that developing new tools can always be easy.

The various tools in the repository are named after various positions within religious hierarchy. In spirit, these are brothers to the famous Tumblr Saviour.

Because each of these tools operate with the same underlying core, entry can begin with any of the tools. For instance, if you run **monk** first, you will be asked to log in the same way as if you had run **theologian**. Your login is saved (see FAQ about logging out), so any subsequent calls to the program will go right into executing them without asking you to log in again.

## Tools ##

#### Theologian ####
The eponymous tool, theologian (*one who studies to learn the divine*) is responsible for the collection of all tags from all posts across all blogs that the user is currently logged in under. Each tag of every post is saved and categorised in a local SQLite database, which the user is then able to use as they'd like to learn anything about their own tag use. Ability to use the results of theologian requires a minimal level knowledge of SQL and the ability to open SQLite 3 files.

Theologian searches automatically across every blog in the currently authenticated user's account. Therefore, once you have passed the initial Theologian access (where you get sent to Tumblr to press "Allow"), you don't have to do anything else. The command line invocation for Theologian is:


```
#!cmd

python3 theologian.py
```


#### Monk ####
A tool designed for Tumblr roleplayers, monk (*one who reads the divine texts*) is used to "rewind" a roleplay thread from its latest post to its first, and then save this as an HTML file, making it easy for anybody to read the roleplay in a linear fashion, all in one go.

The code for this tool requires that you have the URL for the latest entry in the thread. Technically, it doesn't need to be the *latest* entry, but the way monk works is by retracing the reblog trail from the post you specify until it reaches the original post. Searching the reblogs of the provided post and finding the right "next page" would be nearly impossible, even if the Tumblr API supported doing that easily.

When it comes time to run the program, you need to provide the url as an argument. The command line invocation for this is:


```
#!cmd

python3 monk.py http://harry-potter-role-play.tumblr.com/post/1723894724/harry-looked-at-ron-deviously-snape-is-in-for-a-surprise/
```

The output will, by default, be saved to monk.htm. If you want to have it saved to a specific file, you can provide that filename as an additional command line argument:

```
#!cmd

python3 monk.py http://harry-potter-role-play.tumblr.com/post/1723894724/harry-looked-at-ron-deviously-snape-is-in-for-a-surprise/ snape_gets_it.htm
```

## FAQ ##

#### How do I get started? ####
I decided that Theologian should not all function off of one central Tumblr API key. As such, when you first download the repository, you will **need to [register your own Tumblr OAuth application](https://www.tumblr.com/oauth/apps)**. Once you have done this, copy *config.json-template* to *config.json*. Place your own consumer key and consumer secret into this new JSON. When Theologian attempts to run any of the tools, it will look for *config.json* and load the values from there. If it isn't there, it'll fail; but it'll also be nice about it and tell you to check here.

#### What is 'setup.cygwin' for? ####
I personally use Cygwin. Cygwin by default will attempt to use a command-line browser when opening a URL. That's absolutely no good when it comes to trying to authenticate yourself with Tumblr. So, if you're trying to log in for the first time and you're using Cygwin, first type:


```
#!cmd

source setup.cygwin
```

That'll cause it to open the file in your default (real) browser.

#### How do I log my Tumblr account out? ####
That's a simple one! Once you've logged in, you should see a file in the root directory called "stored_session". Delete that file, and you're all logged out; that's the only place that Theologian stores any authentication data.

## Requirements ##

The Theologian suite is written in Python 3. It utilises the [Tumblpy](https://github.com/michaelhelmick/python-tumblpy) library to make all of its calls to the API. This library is included locally, but there are other dependencies required for the Theologian suite that should be installed in order to run. The following Python 3 library must be installed:

* requests
* [requests_oauthlib](https://github.com/requests/requests-oauthlib)

## Contribution guidelines ##

I'd love to open this up and allow others to contribute as well! Contact me on here by [sending me a message](https://bitbucket.org/account/notifications/send/?receiver=ahlec) and we can talk! If you know my Tumblr, feel free to send me a message there, but if you don't, send me a message via Bitbucket.