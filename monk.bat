@echo off

echo wscript.echo inputbox(WScript.Arguments(0),WScript.Arguments(1)) >"%temp%\input.vbs"
for /f "tokens=* delims=" %%a in ('cscript //nologo "%temp%\input.vbs" "Enter the URL for the last post in the thread" "Theologian: monk"') do set postUrl=%%a

C:\cygwin\bin\bash.exe --login -i -c "cd $(cygpath \"%CD%\"); python3 _pipeCygWin.py monk.py %postUrl%"