import sys
import sqlite3
import datetime
import os
import core_theologian

t = core_theologian.login()
    
############################################
# BLOG STUFF! (YAAYYY)
############################################
user_info = t.post('user/info')

##############################
# DATABASE
##############################
if os.path.isfile(user_info['user']['name'] + '.db'):
    os.remove(user_info['user']['name'] + '.db')
database = sqlite3.connect(user_info['user']['name'] + '.db')
database.execute("""CREATE TABLE blogs(blog_name text primary key not null, blog_title text, blog_url text,
    post_count integer not null, last_updated text not null);""")
database.execute("""CREATE TABLE posts(post_id integer primary key not null, blog_name text not null,
    post_type text not null, post_date text not null, url text, source_url text, notes integer not null);""")
database.execute("""CREATE TABLE tags(tag_id integer primary key autoincrement not null, post_id integer not null,
    tag_name text not null);""")
database.execute("""CREATE VIEW tag_frequency AS
    SELECT tag_name,
           count() AS total_count
      FROM tags
     GROUP BY lower(tag_name)
     ORDER BY total_count DESC;
""")
database.execute("""CREATE VIEW tag_count AS
    SELECT
    p.post_id,
    COUNT(t.tag_name) AS 'total'
    FROM
        posts p
    LEFT JOIN
        tags t
    ON
        p.post_id = t.post_id
    GROUP BY
        p.post_id;""")
database.execute("""CREATE VIEW untagged_posts AS
    SELECT p.post_id,
           p.blog_name,
           p.url
      FROM tag_count tc
           JOIN
           posts p ON tc.post_id = p.post_id
     WHERE tc.total = 0;
""");
database.execute("""CREATE VIEW own_popular_posts AS
    SELECT *
  FROM posts
 WHERE source_url IN (
           SELECT blog_name
             FROM blogs
       )
AND 
       notes > 100
 ORDER BY notes DESC;""");
database.commit()

##############################
# COLLECT INFO!!!
##############################
def get_source_blog(post):
    if 'trail' in post:
        for trail_entry in post['trail']:
            if 'is_root_item' in trail_entry:
                if bool(trail_entry['is_root_item']):
                    return trail_entry['blog']['name']
    if 'source_title' in post and len(str(post['source_title'])) > 0:
        return post['source_title']
    return "<UNDETERMINED>"
    
def get_note_count(post):
    if 'note_count' in post:
        return post['note_count']
    return 0

blog_names = []
for blog in user_info['user']['blogs']:
    blog_names.append("'" + blog['name'] + "'") # blog names can only contain letters, numbers, and hypens; dont need to escape
    print('===== BEGINNING COLLECTION: ' + blog['name'] + ' =======')
    database.execute("""INSERT INTO blogs VALUES(?, ?, ?, ?, ?)""", (blog['name'],
        blog['title'], blog['url'], blog['posts'], datetime.datetime.fromtimestamp(blog['updated']).strftime(
        '%Y-%m-%d %H:%M:%S')))
    sqlite_blog_name = blog['name'].lower().replace('-', '_')
    database.execute("""CREATE VIEW tag_frequency_%s AS
        SELECT tag_name, " ",
           count() AS total_count
      FROM tags t
      JOIN posts p
      WHERE t.post_id = p.post_id AND
          p.blog_name = "%s"
     GROUP BY lower(tag_name)
     ORDER BY total_count DESC""" % (sqlite_blog_name, blog['name']))
    database.commit()
    
    posts = t.get('posts', blog_url=blog['url'])
    number_posts_processed = 0
    while len(posts['posts']) > 0:
        for post in posts['posts']:
            database.execute("""INSERT INTO posts VALUES(?, ?, ?, ?, ?, ?, ?);""", (post['id'], blog['name'],
                post['type'], post['date'], post['post_url'], get_source_blog(post), get_note_count(post)))
            for tag in post['tags']:
                database.execute("""INSERT INTO tags(post_id, tag_name) VALUES(?, ?);""", (post['id'], tag))
            number_posts_processed += 1
            print("[%s, %d of %d] %s" % (blog['name'], number_posts_processed, blog['posts'], post['type']))
        posts = t.get('posts', blog_url=blog['url'], params = {'offset': number_posts_processed })
    database.commit()
    
blog_list = ",".join(blog_names)
database.execute("""CREATE VIEW your_posts AS
    SELECT post_id,
           post_type,
           url,
           blog_name,
           source_url
      FROM posts
     WHERE source_url IN (%s);
""" % blog_list)

database.close()