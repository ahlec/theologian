# THIS IS THE CORE OF THEOLOGIAN!
# DON'T EDIT THIS UNLESS YOU'RE ABSOLUTELY SURE YOU WANT TO BE EDITING
# THE CORE OF THEOLOGIAN. IF YOU WANT TO BE USING IT, CHECK OUT ONE OF
# THE TOOLS TO SEE HOW IT IS BEING USED/CALLED.
from tumblpy import Tumblpy
import urllib.parse
import socket
import sys
import webbrowser
import os
import json
import random

def login():
    # CONFIG
    DEBUG_MESSAGES = False
    STORE_SESSION = True
    # END CONFIG

    #############
    # LOAD OAUTH CONSUMER CONFIG
    #############
    if not os.path.isfile("config.json"):
        print("please create a 'config.json' file using the 'config.json-template'. Consult the README for more information (FAQ).")
        sys.exit(1)
    configFile = open("config.json", "r")
    config = json.load(configFile)
    CONSUMER_KEY = config["key"]
    CONSUMER_SECRET = config["secret"]
    configFile.close()

    hasStoredSession = False
    if STORE_SESSION and os.path.isfile('stored_session'):
        stored_session = open('stored_session', 'rt')
        OAUTH_TOKEN = stored_session.readline().strip()
        OAUTH_TOKEN_SECRET = stored_session.readline().strip()
        stored_session.close()
        return Tumblpy(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    else:
        t = Tumblpy(CONSUMER_KEY, CONSUMER_SECRET)
        
        #####################################
        # Authorize
        # This will require the user to go to a URL in their browser. We'll set a callback
        # to go to a certain port in localhost, and then we'll listen on that port in order
        # to retrieve the token callback information
        #####################################
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        portNumber = 1717   # you should know me that 17 is the best number that god ever created
        hasFoundPortNumber = False
        generationAttempts = 0
        while not hasFoundPortNumber:
            try:
                s.bind(("", portNumber))
                hasFoundPortNumber = True
            except socket.error as msg:
                if DEBUG_MESSAGES:
                    print("Bind failed on port", portNumber,". Trying again")
                if generationAttempts >= 17:
                    print("Failed too many times to bind to a socket. Sorry, we're ending to prevent an infinite loop.")
                    sys.exit(1)
                portNumber = random.randint(1000, 65535) # let's just completely bypass all of the common known ports
        if DEBUG_MESSAGES:
            print("Binding to port", portNumber)
                
        auth_props = t.get_authentication_tokens(callback_url='http://127.0.0.1:%s' % portNumber)
        auth_url = auth_props['auth_url']

        OAUTH_TOKEN_SECRET = auth_props['oauth_token_secret']

        webbrowser.open(auth_url, new=2)
        if DEBUG_MESSAGES:
            print("Connect with Tumblr via %s" % auth_url)

        # Set up the port listening
        s.listen(10)
        conn, addr = s.accept()

        # Get the request string
        data = conn.recv(1024).decode('utf-8')
        end = data.index(" HTTP/1.1")
        requestString = data[6:end] # 6 is from "GET /?"
        s.close()
        query_string = urllib.parse.parse_qs(requestString)

        ############################################
        # RE-INITIALISE SO THAT WE CAN CONTINUE
        ############################################
        if DEBUG_MESSAGES:
            print(query_string["oauth_token"][0])
            print(query_string["oauth_verifier"][0])
        t = Tumblpy(CONSUMER_KEY, CONSUMER_SECRET, query_string["oauth_token"][0], OAUTH_TOKEN_SECRET)
        authorized_tokens = t.get_authorized_tokens(query_string["oauth_verifier"][0])

        if STORE_SESSION:
            stored_session = open('stored_session', 'w')
            stored_session.write(authorized_tokens['oauth_token'] + '\n')
            stored_session.write(authorized_tokens['oauth_token_secret'] + '\n')
            stored_session.close()

        return Tumblpy(CONSUMER_KEY, CONSUMER_SECRET, authorized_tokens['oauth_token'], authorized_tokens['oauth_token_secret'])
        
def getOutputDir():
    configFile = open("config.json", "r")
    config = json.load(configFile)
    configFile.close()
    return config["outputDir"]