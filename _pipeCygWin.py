import os
import subprocess
import sys

os.environ['BROWSER'] = 'cygstart';
pipedCommand = "python3"
for index, value in enumerate(sys.argv):
    if index == 0:
        continue
    pipedCommand = pipedCommand + ' "' + value + '"'
subprocess.call(pipedCommand, shell=True)